import java.util.Scanner;
import java.util.concurrent.locks.Lock;

public class ApplianceStore {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Microwave[] appliances = new Microwave[4];

        for (int i = 0; i < appliances.length; i++) {

            appliances[i] = new Microwave(5, 5, true);

        }

        int weight = scan.nextInt();
        int height = scan.nextInt();

        boolean lock = scan.nextBoolean();
        String temp = scan.nextLine();

        appliances[3].setHeight(height);
        appliances[3].setWeight(weight);
        appliances[3].isOn(temp);
        appliances[3].setLock(lock);

        Microwave.cooking();
        appliances[0].isOpened();
        String tempo = scan.nextLine();
        appliances[2].isOn(tempo);
    }
}