import java.util.Scanner;

public class Microwave {
    private int weight;
    private int height;
    private boolean lock;
    private boolean status;

    public static void cooking() {
        System.out.println("The microwave is cooking");
    }
    //helper method
    private static boolean validate(String values) {
        boolean turnedStatus;
        Scanner Scan = new Scanner(System.in);
        while (values != "true" && values != "false") {
            System.out.println("enter a true or false ");
            values = Scan.next();
        }
        if (values == "true")
            turnedStatus = true;

        else
            turnedStatus = false;

        return turnedStatus;
    }
    
    public void isOn(String theKey) {
    
       this.status=validate(theKey);
    
    }
    
    public void setWeight(int weight){
        this.weight=weight;
    }
    public void setHeight(int height){
        this.height=height;
    }
    public void setLock(boolean lock){
        this.lock=lock;
    }
    
    public int getWeight(){
       return this.weight;
    }
    public int getHeight(){
        return this.height;
     }
    public boolean getLock(){
        return this.lock;
     }
    public Microwave(int height,int weight,boolean lock){
        this.height=height;
        this.weight=weight;
        this.lock=lock;
    }
    public void isOpened() {
        if (lock == true)
            System.out.println("The microwave is locked");
        else
            System.out.println("The microwave is unlocked");
    }

}
